#pragma semicolon 1
#include <sdktools>
#pragma newdecls required
#include <warden>
#include <sm_jail_redie>
#define IS_CLIENT(%1)	(1 <= %1 <= MaxClients)

int g_offsCollisionGroup		= -1;

Handle h_Timer_NoBlock[MAXPLAYERS+1];

public Plugin myinfo = 
{
	name = "[CS:GO] Jail no-block",
	author = "ShaRen",
	version = "1.0.1",
	url = "servers-info.ru"
}

public void OnPluginStart()
{
	/*Для NoBlock'а*/
	g_offsCollisionGroup = FindSendPropInfo("CBaseEntity", "m_CollisionGroup");
	if (g_offsCollisionGroup == -1)
		SetFailState("[Warden] Failed to get offset for CBaseEntity::m_CollisionGroup.");

	HookEvent("player_spawn", player_spawn, EventHookMode_Post);
	
	HookEntityOutput("trigger_teleport", "OnEndTouch", Teleport_OnEndTouch);
}


public Action player_spawn(Handle event, char[] name, bool silent) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (h_Timer_NoBlock[client] != INVALID_HANDLE) {
		KillTimer(h_Timer_NoBlock[client]); 
		h_Timer_NoBlock[client] = INVALID_HANDLE;
	}
	if (IsClientInGame(client) && IsPlayerAlive(client) && !IsPlayerGhost(client) ) {
		if (!warden_isnoblock()) {
			SetEntData(client, g_offsCollisionGroup, 17, 4, true);
			//PrintToChatAll("g_offsCollisionGroup, 17");
			h_Timer_NoBlock[client] = CreateTimer(3.5, Timer_NoBlock, client);
		} else SetEntData(client, g_offsCollisionGroup, 2, 4, true);
	}
}


public void Teleport_OnEndTouch(const char[] output, int caller, int activator, float delay)
{
	if (!warden_isnoblock() && IS_CLIENT(activator) && IsClientConnected(activator)) {
		int client = activator;
		if (h_Timer_NoBlock[client] != INVALID_HANDLE)  {
			KillTimer(h_Timer_NoBlock[client]); 
			h_Timer_NoBlock[client] = INVALID_HANDLE;
		}
		if ( IsClientInGame(client) && IsPlayerAlive(client) && !IsPlayerGhost(client) ) {
			SetEntData(client, g_offsCollisionGroup, 17, 4, true);
			//PrintToChatAll("%N g_offsCollisionGroup, 17", client);
			h_Timer_NoBlock[client] = CreateTimer(1.0, Timer_NoBlock, client);
		}
	}
}


public Action Timer_NoBlock(Handle hTimer, any client)
{
	h_Timer_NoBlock[client] = INVALID_HANDLE;
	
	if ( IsClientInGame(client) && IsPlayerAlive(client) && !IsPlayerGhost(client)) {
		SetEntData(client, g_offsCollisionGroup, 5, 4, true);
		//PrintToChatAll("%N g_offsCollisionGroup, 5", client);
	}
}